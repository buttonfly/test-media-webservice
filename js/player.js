
/**
 * 
 */
'use strict'

/**
 * 
 */
var player = function ($) { 
	'use strict';
	var _video=null,
	_uri = null,
	videoElement = function() {
		var video = document.getElementById('player');
    	if(video===undefined || video===null) {
    		video = document.createElement('video');
    		if(video!==undefined && video!==null) {
        		video.id='player';
        		video.width = window.innerWidth;
        		video.height = window.innerHeight;
        		video.controls = true;
        		video.autoplay = true;
//        		video.setAttribute('width', window.innerWidth);
//        		video.setAttribute('height', window.innerHeight);
//        		if(document.body!==null) {
//            		document.getElementsByTagName('body')[0];
//        		}
       			// video.ontimeupdate = function() {
	         //    	console.log(video.currentTime + ' / ' + video.duration);
	         //    };

	         //    video.oncanplay = function() {
	        	// 	console.log('oncanplay');
	        	// };
     //    		video.onprogress = function() {
					// console.log('onprogress');
     //    		};
     //    		video.onended = function() {
     //    			console.log('onended');
     //    		};
     //    		video.onloadstart = function() {
     //    			console.log('onloadstart');
     //    		};
     //    		video.ondurationchange = function() {
     //    			console.log('ondurationchange');
     //    		};
     //    		video.onloadedmetadata = function() {
     //    			console.log('onloadedmetadata');
     //    		};
     //    		video.onloadeddata = function() {
     //    			console.log('onloadeddata');
     //    		};
     //    		video.oncanplaythrough = function() {
     //    			console.log('oncanplaythrough');
     //    		};


        		if(document.body!==null) document.body.appendChild(video);
    		}
    	}
    	return video;
	};
	return {
		init: function() {
			var video = videoElement();
	    	if(video===undefined || video===null) {
	    		video = document.createElement('video');
	    		if(video!==undefined && video!==null) {
	        		video.setAttribute('id', 'player');
	        		document.body.appendChild(video);
	    		}
	    	}
		},
		uri: function() {
			return _uri;
		},
	    setUri: function(uri) {
	    	_uri = uri;
	    	var video = videoElement();
	    	if(video!==undefined && video!==null) {
	        	var source = document.getElementById('videoSource');
	        	if(source===undefined || source===null) {
		        	source = document.createElement('source');
		        	source.id = 'videoSource';
		        	video.appendChild(source);
	        	}
	            //source.src = uri;
	        	video.pause();
	        	// source.setAttribute('src', uri);
	        	source.src = uri;
	            video.load();
	    	}
	    },
	    play: function() {
	    	var video = videoElement();
	    	if(video!==undefined && video!==null) video.play();
	    },
	    pause: function() {
	    	var video = videoElement();
	    	if(video!==undefined && video!==null) video.pause();
	    },
	    videoElement: function() {
	    	return videoElement();
	    },
	};
};
if(typeof(define) !== 'undefined' && typeof(define.amd) !== 'undefined') {
    define(["../jquery"], player);
} else {
    // window.colors is defined by the colors.js file; if AMD is not used, it must be included BEFORE hue.js
    window.player = player(window.jQuery);
}

